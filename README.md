# Data Exploration, Analysis and Treatment

Pokemon exploration and data processing

<div align="center">
  <img src="./img/img_pokespartans.png" height="30px">
  <b>Pokedex Project</b>
</div>

---

<div align="center">
    <a href="./LICENSE">
	<img src="https://img.shields.io/badge/License-GPLv3-blue.svg"  alt="license badge">
    </a>
</div>
<div align="center">
    <a href="https://github.com/KorKux1">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/KorKux1?label=KorKux1&style=social">
    </a>
    <a href="https://github.com/eocode">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/eocode?label=eocode&style=social">
    </a>
</div>
<div align="center">
    <a href="https://twitter.com/eocode">
        <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=eocode&style=social&url=https%3A%2F%2Ftwitter.com%2Feocode">
    </a>
</div>

## Notebooks Structure

- [Obtaining Test Data](./Notebooks/Test_Data_Extraction_for_Frontend.ipynb)
- [Obtaining the Images of the Pokemon](./Notebooks/Images_and_Descriptions_Scraper.ipynb)
- [Cleaning the Data](./Notebooks/Clean_Data_Data_Export.ipynb)
- [Exploring the Data](./notebooks/Data_Exploration.ipynb)
- [Load Data to Data WareHouse](./Data/../Notebooks/Load_data_to_Data_WareHouse.ipynb)

## How to contribute

- Review the [code of conduct](ttps://gitlab.com/pokespartans/code-of-conduct) to contribute to the project
- Create a Pull request to the project

## License

GNU GENERAL PUBLIC LICENSE Version 3
